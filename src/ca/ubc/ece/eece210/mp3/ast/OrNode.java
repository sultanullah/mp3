package ca.ubc.ece.eece210.mp3.ast;

import java.util.HashSet;
import java.util.Set;

import ca.ubc.ece.eece210.mp3.Element;
import ca.ubc.ece.eece210.mp3.Catalogue;

public class OrNode extends ASTNode {

    public OrNode(Token token) {
	super(token);
    }
    
    /**
	 * Interpret/evaluate an ORNode of a query over a given catalogue.
	 * the OrNode returns the union of its children nodes. 
	 *
	 * @param Catalogue 
	 * 			catalogue to run the query and evaluate to find the results.
	 * @return a set of elements containing all the albums found when the query uses
	 * 			the given catalogue to evaluate the union of the OrNodes children.
	 */
    @Override
    public Set<Element> interpret(Catalogue argument) {
    	// TODO Auto-generated method stub
		//set to be returned once interpret() has finished
    	Set<Element> mySet = new HashSet<Element>();
    	//check to see if the OrNode has only one child.
    	if(this.children.size() == 1){
    		//call the corresponding interpret() method for the child.
    		mySet.addAll(this.children.get(0).interpret(argument));				
    	}
    	//call the corresponding interpret() method for the child.
    	if(this.children.size() == 2){
    		//call interpret() on each of the children and store the sets in two different sets.
    		Set<Element> holdSet1 = this.children.get(0).interpret(argument);
    		Set<Element> holdSet2 = this.children.get(1).interpret(argument);
    		//evaluate the union of sets returned by the two children.
    		holdSet1.addAll(holdSet2);
    		//add all values of the union to the set that will be returned.
    		mySet.addAll(holdSet1);
    	}
    	//return the set containing the union.
    	return mySet;
    	}
    }