package ca.ubc.ece.eece210.mp3.ast;

import java.util.HashSet;
import java.util.Set;

import ca.ubc.ece.eece210.mp3.Element;
import ca.ubc.ece.eece210.mp3.Catalogue;
import ca.ubc.ece.eece210.mp3.Genre;

public class InNode extends ASTNode {
		
	public InNode(Token token) {
		super(token);
	}
	
	/**
	 * Interpret/evaluate an InNode of a query over a given catalogue.
	 * the InNode returns all the albums inside the genre specified in the query.
	 * The albums of the sub-genres of the genre in the query will also be returned. 
	 *
	 * @param Catalogue 
	 * 			catalogue to run the query and evaluate to find the results.
	 * @return a set of elements containing all the albums found when the query uses
	 * 			the given catalogue to evaluate the albums inside the genre or sub-genre of the genre given.
	 */
	@Override
	public Set<Element> interpret(Catalogue argument) {
		// TODO Auto-generated method stub
		//set to be returned once interpret() has finished
		Set<Element> mySet = new HashSet<Element>();
		//iterate through all genres inside the given Catalogue
		for(int i = 0; i < argument.size(); i++){
			//check to see if the current genre is the genre we are searching for
			Genre tempGenre = (Genre) argument.get(i);
			if(tempGenre.getTitle().equals(this.arguments)){
				//iterate through all albums and sub-genres albums inside the current genre
				//and add it to the set that will be returned
				for(int m = 0; m < tempGenre.getChildren().size(); m++){
					if(!(tempGenre.getChildren().get(m).hasChildren())){
						mySet.add(tempGenre.getChildren().get(m));
					} else {
						mySet.addAll(CheckSubGenre((Genre)tempGenre.getChildren().get(m)));
					}
				}
			} else {
				//if the current genre is not the genre we are searching for
				//iterate through the rest of the "root" genres inside the catalogue and search
				//for the genre in the query using recursion.
				for(int p = 0; p < tempGenre.getChildren().size(); p++){
					if((tempGenre.getChildren().get(p).hasChildren())){
						Catalogue tempCat = new Catalogue();
						tempCat.add(tempGenre.getChildren().get(p));
						mySet.addAll(this.interpret(tempCat));
					} else {}
				}
			}
		}
		//return the set containing all the albums.
		return mySet;
	}

	/**
	 *Helper method that returns a set of all the albums inside a sub-genre only if
	 *the parent genre of the sub-genre is the genre specified in the search query.
	 *
	 * @param genre 
	 * 			.
	 * @return a set of elements containing all the albums found when the query uses
	 * 			the given catalogue to evaluate the albums inside a sub-genre of the specified genre.
	 */
	private Set<Element> CheckSubGenre(Genre genre) {
		//set to be returned containing all the albums inside the sub-genre.
		Set<Element> TempSet = new HashSet<Element>();
		//iterate through the sub-genre and add all album inside the sub-genre.
		for(int n = 0; n < genre.getChildren().size(); n++){
			if(!(genre.getChildren().get(n).hasChildren())){
				TempSet.add(genre.getChildren().get(n));
			} else {
				//if there is a sub-genre inside the sub-genre recursively get all albums
				//inside that sub-genre.
				TempSet.addAll(CheckSubGenre((Genre) genre.getChildren().get(n)));
			}
		}
		//return the set with all the albums within the sub-genre.
		return TempSet;
		}
	}