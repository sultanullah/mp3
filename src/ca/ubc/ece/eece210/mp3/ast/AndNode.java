package ca.ubc.ece.eece210.mp3.ast;

import java.util.HashSet;
import java.util.Set;

import ca.ubc.ece.eece210.mp3.Element;
import ca.ubc.ece.eece210.mp3.Catalogue;

/**
 * 
 * @author Sathish Gopalakrishnan
 * 
 */

public class AndNode extends ASTNode {

	/**
	 * Create a new AndNode given a parser token
	 * 
	 * @param token
	 */
	public AndNode(Token token) {
		super(token);
	}

	/**
	 * Interpret/evaluate an ANDNode of a query over a given catalogue.
	 * the ANDNode returns the intersection of its children nodes. 
	 *
	 * @param Catalogue 
	 * 			catalogue to run the query and evaluate to find the results.
	 * @return a set of elements containing all the albums found when the query uses
	 * 			the given catalogue to evaluate the intersection of the ANDNodes children.
	 */
	@Override
	public Set<Element> interpret(Catalogue catalogue) {
		// TODO Auto-generated method stub
		//set to be returned once interpret() has finished.
		Set<Element> mySet = new HashSet<Element>();
		//check to see if the ANDNode has only one child.
		if(this.children.size() == 1){
			//call the corresponding interpret() method for the child.
			mySet.addAll(this.children.get(0).interpret(catalogue));				
		}
		//check to see if the ANDNode has two children.
		if(this.children.size() == 2){
			//call interpret() on each of the children and store the sets in two different sets.
			Set<Element> holdSet1 = this.children.get(0).interpret(catalogue);
			Set<Element> holdSet2 = this.children.get(1).interpret(catalogue);
			//evaluate the intersection of sets returned by the two children.
			holdSet1.retainAll(holdSet2);
			//add all values of the intersection to the set that will be returned.
			mySet.addAll(holdSet1);
			
		}
		//return the set containing the intersection.
		return mySet;
		}
	}