package ca.ubc.ece.eece210.mp3.ast;


import java.util.HashSet;
import java.util.Set;


import ca.ubc.ece.eece210.mp3.Album;
import ca.ubc.ece.eece210.mp3.Catalogue;
import ca.ubc.ece.eece210.mp3.Element;

public class ByNode extends ASTNode {

	public ByNode(Token token) {
		super(token);
	}
	
	/**
	 * Interpret/evaluate an ByNode of a query over a given catalogue.
	 * the ByNode returns a set of elements containing all albums in the catalogue
	 * that have the performer specified in the query. 
	 *
	 * @param Catalogue 
	 * 			catalogue to run the query and evaluate to find the results.
	 * @return a set of elements containing all the albums found when the query uses
	 * 			the given catalogue to find all the albums with the same performer.
	 */
	@Override
	public Set<Element> interpret(Catalogue argument) {
		// TODO Auto-generated method stub
		//set to be returned once interpret() has finished
		Set<Element> mySet = new HashSet<Element>();
		//iterate through all genres inside the given Catalogue
		for(int i = 0; i < argument.size(); i++){
			//iterate through all albums and sub-genres inside one genre of the catalogue
			for(int k = 0;  k < argument.get(i).getChildren().size(); k++ ){
				//check if the element of the "root" genre is an album
				if(!(argument.get(i).getChildren().get(k).hasChildren())){
					Album tempAlbum = (Album) argument.get(i).getChildren().get(k);
					//if the performer of the album is equal to the performer specified in the query,
					//add it to the set to be returned.
					if(tempAlbum.getPerformer().equals(this.arguments)){
						mySet.add(tempAlbum);
					}
				}
				//check if the element of the "root" genre is a sub-genre
				if(argument.get(i).getChildren().get(k).hasChildren()){
					//recursively call interpret() and check if the performer of the albums in the
					//sub-genres are equal to the performer specified.
					Catalogue tempCat = new Catalogue();
					tempCat.add(argument.get(i).getChildren().get(k));
					mySet.addAll(this.interpret(tempCat));
				}
			}			
		}
		//return set containing all albums with the same performer.
		return mySet;
	}
	
}