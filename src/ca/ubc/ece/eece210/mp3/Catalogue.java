package ca.ubc.ece.eece210.mp3;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import ca.ubc.ece.eece210.mp3.ast.ASTNode;
import ca.ubc.ece.eece210.mp3.ast.QueryParser;
import ca.ubc.ece.eece210.mp3.ast.QueryTokenizer;
import ca.ubc.ece.eece210.mp3.ast.Token;

/**
 * Container class for all the albums and genres. Its main responsibility is to
 * save and restore the collection from a file.
 * 
 * @author Sathish Gopalakrishnan
 * 
 */
public final class Catalogue {

	private List<Element> contents;

	/**
	 * Builds a new, empty catalogue.
	 */
	public Catalogue() {
		contents = new ArrayList<Element>();
	}

	public int size() {
		return contents.size();
	}
	
	public Element get(int index) {
		return contents.get(index);
	}
	
	public void add(Element e) {
		contents.add(e);
	}

	/**
	 * Builds a new catalogue and restores its contents from the given file.
	 * 
	 * @param fileName
	 *            the file from where to restore the library.
	 */
	public Catalogue(String fileName) {
		// TODO implement
		// HINT:  look at Genre.restoreCollection(...)
	}

	/**
	 * Saved the contents of the catalogue to the given file.
	 * 
	 * @param fileName
	 *            the file where to save the library
	 */
	public void saveCatalogueToFile(String fileName) {
		// TODO implement
	}
	/**
	 * Evaluates a query string on a Catalogue object and returns a list of albums
	 * that correspond to the given query. The search uses the words by(performer), in(genre),
	 * matches(album name), and And and Or combinations to search for a specific list of albums. 
	 *
	 * @param queryString 
	 * 			string that contains a query to be searched through the catalogue.
	 * @return a list of albums that share the property/properties that are being search using
	 * 			the search query.
	 */	
	public List<Album> query(String queryString) {
		//list that will hold all the albums found.
		List<Album> tempList = new ArrayList<Album>();
		//tokenize the input query string.
		List<Token> tokens = QueryTokenizer.tokenizeInput(queryString);
		//parse through the tokens using QueryParser.
		QueryParser parser = new QueryParser(tokens);
		//create an Abstract Syntax Tree.
		ASTNode tree = parser.getRoot();
		//set of albums when interpret is called on the root node of the tree.
		Set<Element> elementSet = tree.interpret(this);
		//iterate through the set of albums and add each album to the list that will be
		//returned.
		Iterator<Element> Itr = elementSet.iterator();
		while (Itr.hasNext()) {
			tempList.add((Album) Itr.next());
		}		
		//return list containing all albums.				
		return tempList;
	}
}