package ca.ubc.ece.eece210.mp3.ast;

import static org.junit.Assert.*;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import ca.ubc.ece.eece210.mp3.Album;
import ca.ubc.ece.eece210.mp3.Catalogue;
import ca.ubc.ece.eece210.mp3.Genre;

/**
 * test complex queries that uses ALL AST subclasses
 */
public class Test3 {
	
	@Test
	public void test1() {

		ArrayList<String> SongList3 = new ArrayList<String>();
		
		SongList3.add("Bad");
		SongList3.add("Speed Demon");
		
		Album Bad = new Album("Bad", "MJ", SongList3);
		
		ArrayList<String> SongListchan = new ArrayList<String>();
		
		SongListchan.add("Stairway to China");
		SongListchan.add("ChinkityChong");
		
		Album ChunkyRifts = new Album("ChunkyRifts", "CHINGCHAN", SongListchan);
		
		ArrayList<String> SongList1 = new ArrayList<String>();
		
		SongList1.add("Triller");
		SongList1.add("Beat It");
		
		Album Thriller = new Album("Thriller", "MJ", SongList1);
				
		ArrayList<String> SongList2 = new ArrayList<String>();
		
		SongList2.add("PorcupineBeats");
		SongList2.add("Revolver");
		
		Album MeatBallPop = new Album("Bad", "Wakatang", SongList2);
				
		Genre Pop = new Genre("Pop");
		Genre FunkPop = new Genre("FunkPop");
		Genre Rock = new Genre("Rock");
		Genre NewFunkPop = new Genre("NewFunkPop");
	

		Thriller.setGenre(Pop);
		MeatBallPop.setGenre(FunkPop);
		ChunkyRifts.setGenre(Rock);
		Bad.setGenre(NewFunkPop);
		Pop.addToGenre(FunkPop);
		FunkPop.addToGenre(NewFunkPop);
				
		Catalogue testcat = new Catalogue();
			
		testcat.add(Pop);
		testcat.add(Rock);
		
		// Above this line is a sample catalogue made.
		// The catalogue looks like this:
		//
		//
		//						Pop(Genre)																			Rock(Genre)
		//				/					\																			|
		//		Thriller(Album)		FunkPop(SubGenre)																ChunkyRifts(Album)
		//									|				\
		//							MeatBallPop(Album)	NewFunkPop(SubGenre)
		//														|
		//												Bad(Album)
		//
		//
		
		
		// Thriller and Bad share the same artist. MeatBallPop and ChunkyRifts have different artists.
		// Our search query will search for the Album "Bad" by MJ inside Pop. Note that I've made MeatBallPop's album title to also be "Bad", so there are two "Bad" albums with two different artists.
		// Our query should only pick out the "Bad" by MJ. We will also search via <Or> for ChunkyRifts in another genre.
		
		// We make the album list for our expected outputs, in this case, Bad and ChunkyRifts.
		List<Album> holdAlbum = new LinkedList<Album>();
		holdAlbum.add(Bad);
		holdAlbum.add(ChunkyRifts);
		
		// We test and compare our Album list and actual output.
		assertEquals(holdAlbum, testcat.query("in (\"Pop\") && matches (\"Bad\") && by (\"MJ\") || matches (\"ChunkyRifts\")"));
		
	}
	
}