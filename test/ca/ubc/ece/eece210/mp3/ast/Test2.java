package ca.ubc.ece.eece210.mp3.ast;
import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.junit.Test;
import ca.ubc.ece.eece210.mp3.Album;
import ca.ubc.ece.eece210.mp3.Catalogue;
import ca.ubc.ece.eece210.mp3.Genre;
/**
 * test non-leaf node ( AndNode and OrNode ) that tests interpret functionality
 */
public class Test2 {
	@Test
	public void test_AndNode() {
		
		ArrayList<String> SongList1 = new ArrayList<String>();
		
		SongList1.add("Triller");
		SongList1.add("Beat It");
		
		Album Thriller = new Album("Thriller", "MJ", SongList1);
				
		ArrayList<String> SongList2 = new ArrayList<String>();
		
		SongList2.add("PorcupineBeats");
		SongList2.add("Revolver");
		
		Album MeatBallPop = new Album("MeatBallPop", "Wakatang", SongList2);
		
		ArrayList<String> SongListchan = new ArrayList<String>();
		
		SongListchan.add("Stairway to China");
		SongListchan.add("ChinkityChong");
		
		Album ChunkyRifts = new Album("MeatBallPop", "CHINGCHAN", SongListchan);
				
		Genre Pop = new Genre("Pop");
		Genre FunkPop = new Genre("FunkPop");
		Genre Rock = new Genre("Rock");
		
		
		Thriller.setGenre(Pop);
		MeatBallPop.setGenre(FunkPop);
		Pop.addToGenre(FunkPop);
		Rock.addToGenre(ChunkyRifts);
		
		Catalogue testcat = new Catalogue();
			
		testcat.add(Pop);
		testcat.add(Rock);
	
		// Above this line is a sample catalog made.
		// The catalog looks like this:
		//
		//
		//						Pop(Genre)									Rock(Genre)							
		//				/					\									|						
		//		Thriller(Album)		FunkPop(SubGenre)						ChunkyRifts(Album)						
		//									|				
		//							MeatBallPop(Album)	
		//												
		//									
		//
		//
		
		
		// Here we are testing AndNode's capability to combine two or more query search properties.
		// In this instance, we'll search for albums that have the title, MeatBallPop, AND authored by Wakatang. 
		// To ensure precision, I'll give ChunkyRifts the same album title as MeatBallPop, however they will have different authors. Our second clause binded with our AND will help
		// ensure we have the correct album.
		// Our output should give one album, MeatBallPop.
	List<Album> holdAlbum = new LinkedList<Album>();
	holdAlbum.add(MeatBallPop);
	
	assertEquals(holdAlbum, testcat.query("matches (\"MeatBallPop\") && by (\"Wakatang\")"));
	
	}
	
	@Test
	public void test_OrNode() {
		
		ArrayList<String> SongList1 = new ArrayList<String>();
		
		SongList1.add("Triller");
		SongList1.add("Beat It");
		
		Album Thriller = new Album("Thriller", "MJ", SongList1);
				
		ArrayList<String> SongList2 = new ArrayList<String>();
		
		SongList2.add("PorcupineBeats");
		SongList2.add("Revolver");
		
		Album MeatBallPop = new Album("MeatBallPop", "Wakatang", SongList2);
		
		ArrayList<String> SongListchan = new ArrayList<String>();
		
		SongListchan.add("Stairway to China");
		SongListchan.add("ChinkityChong");
		
		Album ChunkyRifts = new Album("ChunkyRifts", "CHINGCHAN", SongListchan);
				
		Genre Pop = new Genre("Pop");
		Genre FunkPop = new Genre("FunkPop");
		Genre Rock = new Genre("Rock");
		
		
		Thriller.setGenre(Pop);
		MeatBallPop.setGenre(FunkPop);
		Pop.addToGenre(FunkPop);
		Rock.addToGenre(ChunkyRifts);
		
		Catalogue testcat = new Catalogue();
			
		testcat.add(Pop);
		testcat.add(Rock);
	
		// Above this line is a sample catalog made.
		// The catalog looks like this:
		//
		//
		//						Pop(Genre)									Rock(Genre)							
		//				/					\									|						
		//		Thriller(Album)		FunkPop(SubGenre)						ChunkyRifts(Album)						
		//									|				
		//							MeatBallPop(Album)	
		//												
		//									
		//
		//
		
		
		// Here we are testing OrNode's capability to widen a query search with extra clauses.
		// In this case, I'm going to search for albums authored by MJ or CHINGCHAN. Our output should be only two albums, Thriller and ChunkyRifts.
	
	List<Album> holdAlbum = new LinkedList<Album>();
	holdAlbum.add(Thriller);
	holdAlbum.add(ChunkyRifts);
	
	
	assertEquals(holdAlbum,testcat.query("by (\"MJ\") || by (\"CHINGCHAN\")"));
	
	}
	
	
	
}
