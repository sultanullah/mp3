package ca.ubc.ece.eece210.mp3.ast;
import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.junit.Test;
import ca.ubc.ece.eece210.mp3.Album;
import ca.ubc.ece.eece210.mp3.Catalogue;
import ca.ubc.ece.eece210.mp3.Genre;
/**
 * Test class for leaf nodes ( InNode, MatchesNode, ByNodes )
 * also test interpret()
 */
	public class Test1 {
	
		
		@Test
		public void test_inNode() {
			
			
			ArrayList<String> SongList1 = new ArrayList<String>();
			
			SongList1.add("Triller");
			SongList1.add("Beat It");
			
			Album Thriller = new Album("Thriller", "MJ", SongList1);
					
			ArrayList<String> SongList2 = new ArrayList<String>();
			
			SongList2.add("PorcupineBeats");
			SongList2.add("Revolver");
			
			Album MeatBallPop = new Album("MeatBallPop", "Wakatang", SongList2);
			
			ArrayList<String> SongListchan = new ArrayList<String>();
			
			SongListchan.add("Stairway to China");
			SongListchan.add("ChinkityChong");
			
			Album ChunkyRifts = new Album("ChunkyRifts", "CHINGCHAN", SongListchan);
					
			Genre Pop = new Genre("Pop");
			Genre FunkPop = new Genre("FunkPop");
			Genre Rock = new Genre("Rock");
			
			
			Thriller.setGenre(Pop);
			MeatBallPop.setGenre(FunkPop);
			Pop.addToGenre(FunkPop);
			Rock.addToGenre(ChunkyRifts);
			
			Catalogue testcat = new Catalogue();
				
			testcat.add(Pop);
			testcat.add(Rock);
		
			// Above this line is a sample catalog made.
			// The catalog looks like this:
			//
			//
			//						Pop(Genre)									Rock(Genre)							
			//				/					\									|						
			//		Thriller(Album)		FunkPop(SubGenre)						ChunkyRifts(Album)						
			//									|				
			//							MeatBallPop(Album)	
			//												
			//									
			//
			//
			
			
			// Here we are testing InNode's capability to output all albums in a given root genre within the catalog. 
			// I've made a catalog with two root genres, so see if our query only looks through the one genre given, which is Pop.
			//
		
		
			
		// We make a Album list of our expected output albums.
		List<Album> holdAlbum = new LinkedList<Album>();
		holdAlbum.add(Thriller);
		holdAlbum.add(MeatBallPop);
		
		//Here we do our assert test, and if it succeeds we're in the green.
		assertEquals(holdAlbum, testcat.query("in (\"Pop\")"));
		
		}
		
		@Test
		public void test_MatchesNode() {
			
			ArrayList<String> SongList1 = new ArrayList<String>();
			
			SongList1.add("Triller");
			SongList1.add("Beat It");
			
			Album Thriller = new Album("Thriller", "MJ", SongList1);
					
			ArrayList<String> SongList2 = new ArrayList<String>();
			
			SongList2.add("PorcupineBeats");
			SongList2.add("Revolver");
			
			Album MeatBallPop = new Album("kevinchan", "Wakatang", SongList2);
			
			ArrayList<String> SongListchan = new ArrayList<String>();
			
			SongListchan.add("Stairway to China");
			SongListchan.add("ChinkityChong");
			
			Album ChunkyRifts = new Album("kevinchan", "CHINGCHAN", SongListchan);
					
			Genre Pop = new Genre("Pop");
			Genre FunkPop = new Genre("FunkPop");
			Genre Rock = new Genre("Rock");
			
			
			Thriller.setGenre(Pop);
			MeatBallPop.setGenre(FunkPop);
			Pop.addToGenre(FunkPop);
			Rock.addToGenre(ChunkyRifts);
			
			Catalogue testcat = new Catalogue();
				
			testcat.add(Pop);
			testcat.add(Rock);
		
			// Above this line is a sample catalog made.
			// The catalog looks like this:
			//
			//
			//						Pop(Genre)									Rock(Genre)							
			//				/					\									|						
			//		Thriller(Album)		FunkPop(SubGenre)						ChunkyRifts(Album)						
			//									|				
			//							MeatBallPop(Album)	
			//												
			//									
			//
			//
			
			
			// Here we are testing MatchesNode's capability to output all albums in a given catalog that matches the title input.
			// In this case, I've made MeatBallPop and ChunkyRift albums to have the same title, "kevinchan".
			// Then I've made the search query for "kevinchan", and thus our output should be an album list including MeatBallPop and ChunkyRifts.
		
		List<Album> holdAlbum = new LinkedList<Album>();	
		holdAlbum.add(ChunkyRifts);
		holdAlbum.add(MeatBallPop);
		
		assertEquals(holdAlbum, testcat.query("matches (\"kevinchan\")"));
		
	
		}
		
		@Test
		public void test_ByNode() {
			
			ArrayList<String> SongList1 = new ArrayList<String>();
			
			SongList1.add("Triller");
			SongList1.add("Beat It");
			
			Album Thriller = new Album("Thriller", "MJ", SongList1);
					
			ArrayList<String> SongList2 = new ArrayList<String>();
			
			SongList2.add("PorcupineBeats");
			SongList2.add("Revolver");
			
			Album MeatBallPop = new Album("MeatBallPop", "kevinchan", SongList2);
			
			ArrayList<String> SongListchan = new ArrayList<String>();
			
			SongListchan.add("Stairway to China");
			SongListchan.add("ChinkityChong");
			
			Album ChunkyRifts = new Album("ChunkyRifts", "kevinchan", SongListchan);
					
			Genre Pop = new Genre("Pop");
			Genre FunkPop = new Genre("FunkPop");
			Genre Rock = new Genre("Rock");
			
			
			Thriller.setGenre(Pop);
			MeatBallPop.setGenre(FunkPop);
			Pop.addToGenre(FunkPop);
			Rock.addToGenre(ChunkyRifts);
			
			Catalogue testcat = new Catalogue();
				
			testcat.add(Pop);
			testcat.add(Rock);
		
			// Above this line is a sample catalog made.
			// The catalog looks like this:
			//
			//
			//						Pop(Genre)									Rock(Genre)							
			//				/					\									|						
			//		Thriller(Album)		FunkPop(SubGenre)						ChunkyRifts(Album)						
			//									|				
			//							MeatBallPop(Album)	
			//												
			//									
			//
			//
			
			
			// Here we are testing ByNode's capability to find all albums with the given author input.
			// In this case, I've made MeatBallPop and ChunkyRift albums to have the same author, "kevinchan".
			// Then I've made the search query for "kevinchan", and thus our output should be an album list including MeatBallPop and ChunkyRifts.
		
			List<Album> holdAlbum = new LinkedList<Album>();	
			holdAlbum.add(ChunkyRifts);
			holdAlbum.add(MeatBallPop);
			
			assertEquals(holdAlbum, testcat.query("by (\"kevinchan\")"));
			
		
		
	
		}
		
		
}